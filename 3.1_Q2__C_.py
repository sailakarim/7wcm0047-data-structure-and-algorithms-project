# author : Saila Karim
# date : December 2022
# This program sorts the cities list based on length of elements

def merge(left, right):
    # defined an empty list for sorted elements
    merged_cities = []
    # initialize the variables
    i, j = 0, 0
    # Merge sort implementation
    while i < len(left) and j< len(right):
        # check for which element is less with respect to length and append, increment accoridngly
        if len(left[i]) <= len(right[j]):
            merged_cities.append(left[i])
            i = i+1
        else:
            merged_cities.append(right[j])
            j = j+1

    # append merged cities with elements remaining from left and right list
    merged_cities += left[i:]
    merged_cities += right[j:]
    # return the merged cities
    return merged_cities


def mergesort(cities):
    # check if cities list contain only one element
    if len(cities) < 2:
        # return the list
        return cities
    else:
        # get the pivot element by dividing cities length by
        # 2 and converting the floating result if any to int
        pivot = int(len(cities) / 2)
        # recursive call on all elements before pivot element
        left = mergesort(cities[:pivot])
        # recursive call on all elements after pivot element
        right = mergesort(cities[pivot:])
        # merge both left and right and return the sorted list
        return merge(left, right)

cities = [ "Lancaster", "Sunderland", "Wo lv er ha mp to n ", "Nottingham ", " Oxford ", " Plymouth "," Salisbury ", " Salford ", " Wakefield ", "Lichfield ", "Wells "," Preston ", " Brighton Hove ", "St Albans ", " Kingston upon Hull "]
sorted = mergesort(cities)
print(sorted)

