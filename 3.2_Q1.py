
def collatz(n):
    # cheeck for valid n value
    if n <= 0:
        print("Invalid Number")
    else:
        # while n not equal to 1
        while n != 1:
            print(int(n))
            # check for even number and modify n accordingly
            if n % 2 == 0:
                n = n/2
            else:
                n = 3*n + 1

# call function
collatz(100001)