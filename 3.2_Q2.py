from time import perf_counter

def collatz(n):
    # cheeck for valid n value
    if n <= 0:
        print("Invalid Number")
    else:
        # while n not equal to 1
        while n != 1:
            # check for even number and modify n accordingly
            if n % 2 == 0:
                n = n/2
            else:
                n = 3*n + 1

for i in [1,7,15]:
    # start timer
    t1_start = perf_counter()
    # call function
    collatz(i)
    # end timer
    t1_stop = perf_counter()
    # print the seconds we got
    print(f"Elapsed time for (n = {i}): {t1_stop - t1_start} sec")