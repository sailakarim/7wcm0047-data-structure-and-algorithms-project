def frequency_mapping(sentence):
    # initialize empty dictionary where key represent word and
    # value represent frequency of word in given sentence
    frequency = {}
    # get words list
    words = sentence.split(" ")
    # initialize each unique word to frequency of 0
    for word in words:
        frequency[word.lower()] = 0
    # compute words frequency
    for word in words:
        frequency[word.lower()] += 1
    # return frequency
    return frequency

def sorted_mapping(data):
    # initialize dictionary where key is frequency and value is list of all words with matching frequency
    frequency_words = {}
    # get unique values from data
    unique_frequencies = list(set(data.values()))
    # set frequency word for each value to empty list
    for freq in unique_frequencies:
        frequency_words[freq] = []
    # map each word to frequency words dictionary
    for d in data:
        frequency_words[data[d]].append(d)

    # initialize variables
    count = 0
    invalid = False
    print("="*45)
    print("{:<30} {:>10}".format("Word","Frequency"))
    print("=" * 45)
    # loop through sorted frequency words in descending order
    for k in sorted(frequency_words.keys(),reverse=True):
        # sort the given value words list
        frequency_words[k].sort()
        # loop through sorted word list
        for word in frequency_words[k]:
            # print and increment counter
            print("{:<30} {:>10}".format(word,k))
            count += 1
            # check for top 5 words and break both loops
            if count == 5:
                invalid = True
                break
        if invalid:
            break

def main():
    # sentence
    sentences = "The graphic and typographic operators know this well, in reality all the professions dealing with the universe of communication have a stable relationship with these words, but what is it? Lorem ipsum is a dummy text without any sense."
    mapping = frequency_mapping(sentences)
    sorted_mapping(mapping)

if __name__ == "__main__":
    main()
