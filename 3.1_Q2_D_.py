# author : Saila Karim
# date : December 2022
# This program compare two algorithms time complexity and display graph

# import libraries
import time
import matplotlib.pyplot as plt
import random

def insertion_sort(cities):
    # Initialize our result , which will be empty for now
    result = []
    # Initialize our loop variables
    i = 0
    j = 0
    number_of_cities = len( cities ) # len () of a list gives you the number of elements in it
    # len () of a string will give you the length of it
    n = number_of_cities
    number_of_steps = 0
    # Sort using selection sort
    for i in range (0 , number_of_cities ):
        minimum_length = len ( cities [0])
        minimum_element = cities[0]
        for j in range(0, n):
            if (len(cities[j]) < minimum_length):
                minimum_length = len(cities[j])
                minimum_element = cities[j]
        number_of_steps = number_of_steps + 1
        # At the end of the second loop , we will have the shorter element in minimum_element
        # We just need to add it to our results and remove it from our working list
        result.append(minimum_element)
        cities.remove(minimum_element)
        n = n - 1

    # return the sorted list
    return result


def merge(left, right):
    # defined an empty list for sorted elements
    merged_cities = []
    # initialize the variables
    i, j = 0, 0
    # Merge sort implementation
    while i < len(left) and j< len(right):
        # check for which element is less with respect to length and append, increment accoridngly
        if len(left[i]) <= len(right[j]):
            merged_cities.append(left[i])
            i = i+1
        else:
            merged_cities.append(right[j])
            j = j+1

    # append merged cities with elements remaining from left and right list
    merged_cities += left[i:]
    merged_cities += right[j:]
    # return the merged cities
    return merged_cities


def mergesort(cities):
    # check if cities list contain only one element
    if len(cities) < 2:
        # return the list
        return cities
    else:
        # get the pivot element by dividing cities length by
        # 2 and converting the floating result if any to int
        pivot = int(len(cities) / 2)
        # recursive call on all elements before pivot element
        left = mergesort(cities[:pivot])
        # recursive call on all elements after pivot element
        right = mergesort(cities[pivot:])
        # merge both left and right and return the sorted list
        return merge(left, right)


def main():
    merge_sort_time = {}
    insertion_sort_time = {}
    for i in range(1,100):
        data_list = []
        for j in range(10000,500000,i*100):
            probability = random.random()
            round_off = random.randint(1,6)
            data_list.append(str(round(random.randint(1, 9) * probability,round_off)))

        length = len(data_list)
        start_time = time.time()
        work = mergesort(data_list)
        end_time = time.time()
        merge_sort_time[length] = end_time - start_time

        start_time = time.time()
        work  = insertion_sort(data_list)
        end_time = time.time()
        insertion_sort_time[length] = end_time - start_time


    plt.plot(list(insertion_sort_time.keys()),list(insertion_sort_time.values()), linestyle='dotted', label= "Insertion Sort")
    plt.plot(list(merge_sort_time.keys()), list(merge_sort_time.values()), linestyle='dotted',
             label="Merge Sort")
    plt.xlabel("Number of Data Points")
    plt.ylabel("Time Taken In (Sec)")
    plt.legend()
    plt.show()



if __name__ == "__main__":
    main()

