# author : Saila Karim
# date : December 2022
# This program sorts the cities list based on length of elements


def insertion_sort(cities):
    # Initialize our result , which will be empty for now
    result = []
    # Initialize our loop variables
    i = 0
    j = 0
    number_of_cities = len( cities ) # len () of a list gives you the number of elements in it
    # len () of a string will give you the length of it
    n = number_of_cities
    number_of_steps = 0
    # Sort using selection sort
    for i in range (0 , number_of_cities ):
        minimum_length = len ( cities [0])
        minimum_element = cities[0]
        for j in range(0, n):
            if (len(cities[j]) < minimum_length):
                minimum_length = len(cities[j])
                minimum_element = cities[j]
        number_of_steps = number_of_steps + 1
        # At the end of the second loop , we will have the shorter element in minimum_element
        # We just need to add it to our results and remove it from our working list
        result.append(minimum_element)
        cities.remove(minimum_element)
        n = n - 1

    # return the sorted list along with number of elements in the list and number of steps to sort them out
    return result,number_of_cities, number_of_steps



def main():
    cities = input("Enter cities name seperator by comma(,): ")
    # split the names based on comma to get a list
    cities_list = cities.split(",")
    # call the function and get all parameters
    result,number_of_cities, number_of_steps = insertion_sort(cities_list)
    print("The ordered list is:")
    # print the result
    print(str(result))
    print("The list had " + str(number_of_cities) + " cities and I ordered it in " + str(
        number_of_steps) + " steps .")

if __name__ == "__main__":
    main()

